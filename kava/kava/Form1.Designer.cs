﻿namespace kava
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Juoda = new System.Windows.Forms.Button();
            this.Balta = new System.Windows.Forms.Button();
            this.Late = new System.Windows.Forms.Button();
            this.Mocha = new System.Windows.Forms.Button();
            this.Cappuchino = new System.Windows.Forms.Button();
            this.ekranas = new System.Windows.Forms.TextBox();
            this.Reset = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Juoda
            // 
            this.Juoda.Location = new System.Drawing.Point(22, 33);
            this.Juoda.Name = "Juoda";
            this.Juoda.Size = new System.Drawing.Size(358, 59);
            this.Juoda.TabIndex = 0;
            this.Juoda.Text = "Juoda kava";
            this.Juoda.UseVisualStyleBackColor = true;
            this.Juoda.Click += new System.EventHandler(this.Juoda_Click);
            // 
            // Balta
            // 
            this.Balta.Location = new System.Drawing.Point(22, 98);
            this.Balta.Name = "Balta";
            this.Balta.Size = new System.Drawing.Size(358, 59);
            this.Balta.TabIndex = 1;
            this.Balta.Text = "Balta kava";
            this.Balta.UseVisualStyleBackColor = true;
            this.Balta.Click += new System.EventHandler(this.Balta_Click);
            // 
            // Late
            // 
            this.Late.Location = new System.Drawing.Point(22, 163);
            this.Late.Name = "Late";
            this.Late.Size = new System.Drawing.Size(358, 59);
            this.Late.TabIndex = 2;
            this.Late.Text = "Late";
            this.Late.UseVisualStyleBackColor = true;
            this.Late.Click += new System.EventHandler(this.Late_Click);
            // 
            // Mocha
            // 
            this.Mocha.Location = new System.Drawing.Point(22, 228);
            this.Mocha.Name = "Mocha";
            this.Mocha.Size = new System.Drawing.Size(358, 59);
            this.Mocha.TabIndex = 3;
            this.Mocha.Text = "Mocha";
            this.Mocha.UseVisualStyleBackColor = true;
            this.Mocha.Click += new System.EventHandler(this.Mocha_Click);
            // 
            // Cappuchino
            // 
            this.Cappuchino.Location = new System.Drawing.Point(22, 293);
            this.Cappuchino.Name = "Cappuchino";
            this.Cappuchino.Size = new System.Drawing.Size(358, 59);
            this.Cappuchino.TabIndex = 4;
            this.Cappuchino.Text = "Capucchino";
            this.Cappuchino.UseVisualStyleBackColor = true;
            this.Cappuchino.Click += new System.EventHandler(this.Cappuchino_Click);
            // 
            // ekranas
            // 
            this.ekranas.BackColor = System.Drawing.SystemColors.Info;
            this.ekranas.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.ekranas.ForeColor = System.Drawing.Color.MidnightBlue;
            this.ekranas.Location = new System.Drawing.Point(418, 33);
            this.ekranas.Multiline = true;
            this.ekranas.Name = "ekranas";
            this.ekranas.Size = new System.Drawing.Size(307, 319);
            this.ekranas.TabIndex = 5;
            this.ekranas.TabStop = false;
            // 
            // Reset
            // 
            this.Reset.Location = new System.Drawing.Point(627, 391);
            this.Reset.Name = "Reset";
            this.Reset.Size = new System.Drawing.Size(119, 47);
            this.Reset.TabIndex = 6;
            this.Reset.Text = "Reset";
            this.Reset.UseVisualStyleBackColor = true;
            this.Reset.Click += new System.EventHandler(this.Reset_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(758, 450);
            this.Controls.Add(this.Reset);
            this.Controls.Add(this.ekranas);
            this.Controls.Add(this.Cappuchino);
            this.Controls.Add(this.Mocha);
            this.Controls.Add(this.Late);
            this.Controls.Add(this.Balta);
            this.Controls.Add(this.Juoda);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Juoda;
        private System.Windows.Forms.Button Balta;
        private System.Windows.Forms.Button Late;
        private System.Windows.Forms.Button Mocha;
        private System.Windows.Forms.Button Cappuchino;
        private System.Windows.Forms.TextBox ekranas;
        private System.Windows.Forms.Button Reset;
    }
}

